package pascaltree;

public class PascalTask {

    public static void main(String[] args) {
        printPascalTriangle(5);
    }

    public static void printPascalTriangle(int rows) {
        int[][] pascalArray = new int[rows][];
        for (int i = 0; i < rows; i++) {
            pascalArray[i] = new int[i+1];
            for (int j = 0; j < i + 1; j++) {
                if (j == 0 || i == j ) {
                    pascalArray[i][j] = 1;
                } else {
                    pascalArray[i][j] = pascalArray[i - 1][j] + pascalArray[i - 1][j - 1];
                }
            }

        }

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < pascalArray[i].length; j++) {
                System.out.print(pascalArray[i][j] + " ");
            }
            System.out.println();
        }
    }


}
