package treeprint;

public class Node<T> {
    private T data;
    private Node<T> left;
    private Node<T> right;
    private Node<T> parent;

    public Node(T data) {
        this.data = data;
        this.parent = null;
        this.left = null;
        this.right = null;
    }

    public Node(T data, Node<T> parent) {
        this.data = data;
        this.parent = parent;
        this.left = null;
        this.right = null;
    }

    public Node(T data, Node<T> left, Node<T> right) {
        this.data = data;
        this.left = left;
        this.right = right;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Node<T> getLeft() {
        return left;
    }

    public void setLeft(Node<T> left) {
        this.left = left;
    }

    public Node<T> getRight() {
        return right;
    }

    public void setRight(Node<T> right) {
        this.right = right;
    }

    public Node<T> getParent() {
        return parent;
    }

    public void setParent(Node<T> parent) {
        this.parent = parent;
    }

    public boolean isRightChild() {
        if (this.parent!=null)
            return this.parent.getRight().equals(this);
        return false;
    }

    public boolean isLeftChild() {
        if (this.parent!=null)
            return this.parent.getLeft().equals(this);
        return false;
    }

    @Override
    public String toString() {
        return (String) data;
    }
}
