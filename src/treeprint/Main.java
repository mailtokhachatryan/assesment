package treeprint;

import java.util.Arrays;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        Node<String> root = new Node("=");
        Node<String> g = new Node("g", root);
        root.setLeft(g);
        Node<String> minus = new Node("-", root);
        root.setRight(minus);
        Node<String> plus = new Node("+", minus);
        minus.setLeft(plus);
        Node<String> f = new Node("f", plus);
        plus.setLeft(f);
        Node<String> k = new Node("k", plus);
        plus.setRight(k);
        Node<String> up = new Node("^", minus);
        minus.setRight(up);
        Node<String> p = new Node("p", up);
        up.setLeft(p);
        Node<String> minus1 = new Node("-", up);
        up.setRight(minus1);
        Node<String> d = new Node("d", minus1);
        minus1.setLeft(d);
        Node<String> s = new Node("s", minus1);
        minus1.setRight(s);

        Tree<String> tree= new Tree(root);
        Stack stack = new Stack();
        Stack infix = tree.getInfix(root, stack);

        infix.forEach(System.out::print);

    }
}
