package treeprint;

import java.util.Stack;

public class Tree<T> {
    private Node<T> root;

    public Tree(Node<T> root) {
        this.root = root;
    }

    public Stack getInfix(Node<T> root, Stack stack) {
        if (root == null)
            return stack;
        if (root.getLeft() == null) {
//            stack.push("(");
            stack.push(root);
            if (root.getParent() != null && root.isLeftChild())
                stack.push(root.getParent());
            if (root.isRightChild()) {
//                stack.push(")");
                root = root.getParent().getParent();
            }
            root = root.getParent().getRight();
            if (root.getRight() == null && root.getLeft() == null) {
                stack.push(root);
                if (root.getParent().isLeftChild()) {
                    root = root.getParent().getParent().getRight();
                    stack.push(root.getParent());
                } else if (root.getParent().isRightChild() && root.getLeft() == null && root.getRight() == null)
                    return stack;
            }
            root = root.getLeft();
            getInfix(root, stack);
        }
        return getInfix(root.getLeft(), stack);
    }
}
